import { useAuth } from 'store/auth'
import { useAPI } from 'store/api'
import { User, UserUpdate, Group, LoginResponse } from 'components/models'

const baseUrl = 'http://localhost:8000'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type BodyType = Record<string, any>

function authorizationHeaders() {
  return { 'Authorization': `Bearer ${useAuth().getToken}` }
}

function computeFullDisplayName(group: Group): string {
  return group.id
    .split(',')
    .slice(0, -2)
    .map((comp: string) => {
      const [_type,value] = comp.split('=')
      return value
    })
    .join(' ⟨ ')
}

export function baseCall(method: string, path: string, body?: BodyInit, headers?: Record<string, string>): Promise<Response> {
  return new Promise<Response>((resolve, reject) => {
    const h: Headers = headers ? new Headers(headers) : new Headers()
    h.set('Content-type', 'application/json')

    fetch(`${baseUrl}${path}`, { method, body, headers: h })
      .then(res => {
        if(!res.ok) reject(new Error(`Request error ${res.status}: ${res.statusText}`))
        resolve(res)
      })
      .catch(err => reject(new Error(`Network error ${err as string}`)))
  })
}

export function anonCall(method: string, path: string, body?: BodyType, headers?: Record<string, string>): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    baseCall(method, path, JSON.stringify(body), headers)
      .then(_res => resolve())
      .catch(err => reject(err))
  })

}

export function anonFetch<T>(method: string, path: string, body?: BodyType, headers?: Record<string, string>): Promise<T> {
  return new Promise<T>((resolve, reject) => {
    baseCall(method, path, JSON.stringify(body), headers)
      .then(res => {
        res.json()
          .then(json => resolve(json as T))
          .catch(err => reject(new Error(`JSON parse error ${err as string}`)))
      })
      .catch(err => reject(err))
  })
}

export function connectedCall(method: string, path: string, body?: BodyType, headers?: Record<string, string>): Promise<void> {
  return anonCall(method, path, body, { ...authorizationHeaders(), ...headers })
}

export function connectedFetch<T>(method: string, path: string, body?: BodyType, headers?: Record<string, string>): Promise<T> {
  return anonFetch<T>(method, path, body, { ...authorizationHeaders(), ...headers })
}


export function login(username: string, password: string): Promise<void> {
  return new Promise((resolve, reject) => {
    anonFetch<LoginResponse>('POST', '/login', { username, password })
      .then((json) => {
        useAuth().setToken(json.token)
        resolve()
      })
    .catch(err => reject(err))
  })
}

export function logout(): Promise<void> {
  return new Promise((resolve, reject) => {
    connectedCall('POST', '/logout')
      .then((_res) => {
        useAuth().resetToken()
        resolve()
      })
      .catch((err) => reject(err))
  })
}

export function getUser(id: string): Promise<User> {
  return connectedFetch<User>('GET', `/users/${id}`)
}

export function searchUsers(username?: string, searchDN?: string) : Promise<User[]> {
  const params = new URLSearchParams()
  if(username) params.set('username', username)
  if(searchDN) params.set('searchDN', searchDN)

  return new Promise((resolve, reject) => {
    connectedFetch<User[]>('GET', `/users?${params.toString()}`, undefined, {'Content-Type': 'application/x-www-form-urlencoded'})
      .then((json) => resolve(json))
      .catch((err) => reject(err))
  })
}

export function searchGroups(): Promise<Group[]> {
  return new Promise((resolve, reject) => {
    connectedFetch<Group[]>('GET', '/groups')
      .then((json) => resolve(json.map((g: Group) => { return { ...g, fullDisplayName: computeFullDisplayName(g) } })))
      .catch((err) => reject(err))
  })
}

export function createGroup(parent: Group, group: Group): Promise<Group> {
  return new Promise((resolve, reject) => {
    connectedFetch<Group>('POST', '/groups', { ...group, parent: parent.id })
      .then((json) => {
        const newGroup = { ...json, fullDisplayName: computeFullDisplayName(json) }
        useAPI().addGroup(newGroup)
        resolve(newGroup)
      })
      .catch((err) => reject(err))
  })
}

export function deleteGroup(group: Group): Promise<void> {
  return new Promise((resolve, reject) => {
    connectedCall('DELETE', `/groups/${group.id}`)
      .then(() => {
        useAPI().removeGroup(group)
        resolve()
      })
      .catch((err) => reject(err))
  })
}

export function createUser(parent: Group, user: User): Promise<User> {
  return new Promise((resolve, reject) => {
    connectedFetch<User>('POST', '/users', { ...user, parent: parent.id })
      .then((json) => resolve(json))
      .catch((err) => reject(err))
  })
}

export function deleteUser(user: User): Promise<void> {
  return new Promise((resolve, reject) => {
    connectedCall('DELETE', `/users/${user.id}`)
      .then(() => resolve())
      .catch((err) => reject(err))
  })
}

export function updateUser(id: string, data: UserUpdate): Promise<UserUpdate> {
  return new Promise((resolve, reject) => {
    connectedFetch<UserUpdate>('PUT', `/users/${id}`, data)
      .then(updated => resolve(updated))
      .catch(err => reject(err))
  })
}
