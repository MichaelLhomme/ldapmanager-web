import { Notify } from 'quasar'

export function notifyInfo(msg: string) {
  Notify.create({ color: 'green', message: msg, timeout: 1500, progress: true })
}

export function notifyError(msg: string) {
  Notify.create({ color: 'red', icon: 'warning', message: msg, timeout: 3000, progress: true })
}
