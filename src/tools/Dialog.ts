import { Dialog } from 'quasar'

import DialogConfirm from 'components/DialogConfirm.vue'

type ConfirmationCallback = () => Promise<void>


// Ask for confirmation before executing the onSubmit callback
export function withConfirmation(title: string, message: string, onSubmit: ConfirmationCallback): void {
  Dialog.create({
    component: DialogConfirm,
    componentProps: {
      title,
      message,
      onSubmit
    }
  })
}
