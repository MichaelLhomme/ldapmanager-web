import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  // Login without layout
  { name: 'Login', path: '/login', component: () => import('pages/LoginPage.vue') },

  // Other views
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { name: 'Index', path: '', component: () => import('pages/Index.vue') },
      { name: 'Profile', path: '/profile', component: () => import('pages/Profile.vue') },
      { name: 'Users', path: '/users', component: () => import('pages/Users.vue') },
      { name: 'Groups', path: '/groups', component: () => import('pages/Groups.vue') }
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue'),
  },
];

export default routes;
