export interface QForm {
  /** Focus on first focusable element/component in the form */
  focus(): void

  /** Triggers a validation on all applicable inner Quasar components */
  validate(): Promise<boolean>

  /** Resets the validation on all applicable inner Quasar components */
  resetValidation(): void

  /** Manually trigger form validation and submit */
  submit(event?: Event): void

  /** Manually trigger form reset */
  reset(event?: Event): void

  /** Get array of children vue components that support validation */
  getValidationComponents(): any[] // eslint-disable-line @typescript-eslint/no-explicit-any
}
