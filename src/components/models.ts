export interface LoginResponse {
  token: string
}

export interface User {
  id: string
  username: string
  firstName: string
  lastName: string
  password?: string
  sshKey?: string
  email?: string
}

export interface UserUpdate {
  firstName?: string
  lastName?: string
  password?: string
  sshKey?: string
  email?: string
}

export interface Group {
  id: string
  name: string
  description?: string
}

export interface Status {
  status: string
  time: string
  connected: boolean
  connections: number
  openedConnections: number
}
