import { defineStore } from 'pinia'
import * as jose from 'jose'

interface State {
  token: string | null
}

export const useAuth = defineStore('auth', {
  state: () => {
    return {
      token: window.localStorage.getItem('token') || null
    } as State
  },

  getters: {
    getToken(state: State): string { return state.token || '' },
    getPayload(state: State): jose.JWTPayload { return jose.decodeJwt(state.token || '') },
    getUsername(state: State): string {
      // TODO error handling
      const payload = jose.decodeJwt(state.token || '')
      const dn: string = payload.dn as string
      const components: string[] = dn.split(',')
      return components[0].split('=')[1]
    }
  },

  actions: {
    resetToken () {
      this.token = null
      window.localStorage.removeItem('token')
    },

    setToken (token: string) {
      this.token = token
      window.localStorage.setItem('token', token)
    }
  }
})

export type AuthStore = ReturnType<typeof useAuth>
