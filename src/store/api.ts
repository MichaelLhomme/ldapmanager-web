import { defineStore } from 'pinia'

import { Group } from 'components/models'
import { notifyError } from 'tools/Notify'
import { searchGroups } from 'tools/API'

interface State {
  groups: Map<string, Group>
}

export const useAPI = defineStore('api', {
  state: () => {
    return {
      groups: new Map<string, Group>()
    } as State
  },

  getters: {
    getGroup(state: State): (id: string) => Group | undefined { return (id: string) => state.groups.get(id) },
    getGroups(state: State): Group[] {
      // TODO better sort
      return Array.from(state.groups.values())
        .sort((a: Group, b: Group) => {
          // if(a.parent.length < b.parent.length) return -1
          // else if(a.parent.length < b.parent.length) return 1
          if(a.id.length < b.id.length) return -1
          else if(a.id.length < b.id.length) return 1
          else return 0
        })
    }
  },

  actions: {
    addGroup(group: Group): void { this.groups.set(group.id, group) },
    updateGroup(group: Group): void { this.groups.set(group.id, group) },
    removeGroup(group: Group): void { this.groups.delete(group.id) },
    fetchGroups(): void {
      searchGroups()
        .then((json) => {
          this.groups = new Map<string, Group>(json.map((g: Group) => {
            return [ g.id, g]
          }))
        })
        .catch(err => { notifyError(`${err as string}`) })
    }
  }
})

export type APIStore = ReturnType<typeof useAPI>
